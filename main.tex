% !TeX root = ./main.tex
% !TeX spellcheck = en-GB

% Font size
\documentclass[12pt,a4paper,british]{article}

\usepackage{ifxetex}

\ifxetex
  \usepackage{fontspec}
\else
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{babel}
  \usepackage{lmodern}
\fi

\usepackage[margin=1in]{geometry}

\usepackage{hyperref}

% license
\usepackage[
    type={CC},
    modifier={by-nc-sa},
    version={4.0},
]{doclicense}

\usepackage{appendix}

\usepackage{csquotes}

\MakeOuterQuote{"}

\usepackage{epigraph}

% length of epigraph
\setlength\epigraphwidth{12cm}
% remove epigraph bar
\setlength\epigraphrule{0pt}

\renewcommand{\epigraphsize}{\large}

\usepackage{etoolbox}

\makeatletter
\patchcmd{\epigraph}{\@epitext{#1}}{\itshape\@epitext{#1}}{}{}
\makeatother


% for degree symbol
\usepackage{siunitx}

% for chemical formulas
\usepackage[version=4]{mhchem}

% prevent floating figures 
\usepackage{float}

\usepackage{graphicx}

\usepackage[caption=false]{subfig}

% externalize tkiz for faster compiling
\usepackage{tikz}
\usetikzlibrary{external}
\tikzexternalize[prefix=tikz/]

% for plotting graphs
\usepackage{pgfplots} 

% latest compatibility
\pgfplotsset{compat=newest}

% Size of plots
% \pgfplotsset{width=0.\textwidth, height=0.25\textwidth} 

% Thousands separator
\pgfkeys{/pgf/number format/.cd,1000 sep={}}

\usepackage[justification=centering]{caption}

% Referencing
\usepackage[backend=biber,bibstyle=apa,citestyle=authoryear]{biblatex}

\bibliography{references.bib}

% break bilbatex reference URL on number
\setcounter{biburlnumpenalty}{9000}

% packages for Header and Footer
\usepackage{lastpage}
\usepackage{fancyhdr} 
 
%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\title{\textbf{Will the expansion of renewable energy lead to more or less global insecurity?}}

\author{\textit{2270405}}

 % Date, use \date{} for no date
\date{\today}

% save these variables for later use under different name
\makeatletter
\let\doctitle\@title
\let\docauthor\@author
\let\docdate\@date
\makeatother

%----------------------------------------------------------------------------------------
%	HEADER AND FOOTER SECTION
%----------------------------------------------------------------------------------------
\pagestyle{fancy}

% sets both header and footer to nothing
\fancyhf{}

% Header
% remove horizontal header bar
\renewcommand{\headrulewidth}{0pt}
% left hand side header
\lhead{Global Energy Politics - Essay}
% right hand side header
\rhead{\docauthor}
\setlength{\headheight}{15pt}

% Footer
% center of footer
\cfoot{\thepage\ of \pageref{LastPage}}


%------------------------------------------------------------------------

\begin{document}

% Print the title section
\maketitle


\vspace*{\fill}

\epigraph{"The crucial error consists in not seeing that not only growth, but also a zero-growth state, nay, even a declining state which does not converge toward annihilation, cannot exist forever in a finite environment."}{\fullcite[367]{georgescu-roegenEnergyEconomicMyths1975}}


% \begin{quote}
%     \large{
%         "The crucial error consists in not seeing that not only growth, but also a zero-growth state, nay, even a declining state which does not converge toward annihilation, cannot exist forever in a finite environment."
%     }

%     % quote source
%     \begin{flushright}
%         \small{
%             \fullcite[367]{georgescu-roegenEnergyEconomicMyths1975}
%         }
%     \end{flushright}
% \end{quote}

\vspace*{\fill}

\pagebreak

% Introduction
\section*{}

\begin{figure}[H]
    \begin{tikzpicture}
        \begin{axis}[
                ylabel={Mtoe},
                legend pos=north west,
                stack plots=y,
                area style,
                enlarge x limits=false,
                enlarge y limits=false,
                legend style={at={(0.5,-0.1)},anchor=north},
                legend columns=2,
                width=0.5\textwidth
            ]
            \foreach \fuel/\label in {all_renewables_mtoe/Renewables,coalcons_mtoe/Coal,gascons_mtoe/Gas,nuclear_mtoe/Nuclear,oilcons_mtoe/Oil}
                {
                    \addplot table [x=Year, y={\fuel},col sep = comma]{data/bp_2019.csv}
                    \closedcycle;
                    \expandafter\addlegendentry\expandafter{\label}
                }
        \end{axis}
    \end{tikzpicture}
    \begin{tikzpicture}
        \begin{axis}[
                % ylabel={Mtoe},
                yticklabel style={/pgf/number format/fixed},
                legend pos=north west,
                stack plots=y,
                area style,
                enlarge x limits=false,
                enlarge y limits=false,
                legend style={at={(0.5,-0.1)},anchor=north},
                legend columns=2,
                width=0.5\textwidth
            ]
            \foreach \fuel/\label in {hydro_mtoe/Hydro,biofuels_mtoe/Biofuels,geothermal_mtoe/Geothermal,biomass_mtoe/Biomass,solar_mtoe/Solar,wind_mtoe/Wind}
                {
                    \addplot table [x=Year, y={\fuel},col sep = comma]{data/bp_2019.csv}
                    \closedcycle;
                    \expandafter\addlegendentry\expandafter{\label}
                }
        \end{axis}
    \end{tikzpicture}
    \caption{Global Primary Energy consumption by source over time and breakdown of Renewables \\ Data from \protect\textcite{britishpetroleumBPStatisticalReview2019}}
    \label{fig:global-energy-consumption}
\end{figure}

Global Primary Energy consumption over the last 50 years is particularly illustrative of \textcite{georgescu-roegenEnergyEconomicMyths1975}'s point: Figure \ref{fig:global-energy-consumption} is a reminder of our energy consumption's growing dependence on finite supplies of fossil fuels.

Awakening to the inevitable end of these sources of energy, we have begun to develop so-called "Renewable Energies" (Figure \ref{fig:global-energy-consumption}) to counter both the dwindling reserves of fossil fuel as well as the defining characteristic of the anthropocene: Climate Change. Figure \ref{fig:global-energy-consumption} details this expansion, showing increasing diversity of renewable energy sources since the early 2000s after the dominance of Hydroelectric power.

This essay will assess whether this expansion of Renewable Energies will increase or decrease Global Insecurity. We focus on the properties of electric grids and the formation of interdependent "Grid Communities" which consequently reduce conflict. We then study the land and water footprint of Renewables as a factor of increased conflict emanating from the induced resource scarcity. Lastly we explore the Geopolitics of "Critical Raw Materials" and the emerging conflicts over their supply, processing industry and trade. We find that with the expected growth of particular energy sources, the expansion of Renewable Energies will contribute to increasing Global Insecurity.

\section*{Electric Interdependence}

\subsection*{Intermittency and Storage (or lack thereof)}

Electric power generation technologies are spread along a spectrum ranging from "dispatchable" to "intermittent". In essence, some technologies are more or less predictable (controllable) in terms of power output \autocite{teskeEnergyEvolutionSustainable2013}. This is the \textit{raison d'être} of electric grids: splitting the load across as many consumers as possible so that any variation in production or consumption is more like to be offset elsewhere on the network \autocite{jancoviciImpactEnergiesRenouvelables2019}.

Intermittent generation is a key issue of renewable energies: our capacity to predict wind or clouds is not a good as our capacity to control fuel input into an electric power generator which makes renewables volatile sources of power. Furthermore, even a broad electric grid can only mitigate this to a limited degree. Figure \ref{fig:intermitence} illustrates that wind electric production in Spain and German are relatively related, in other words, when there is no wind in Germany there is also no wind in Spain, and conversely.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth,trim={1mm 1mm 1mm 1mm},clip=true]{static/ger-es_wind.jpg}
    \caption{Hourly load on Spanish and German wind farms in 2016 \protect\autocite{jancoviciImpactEnergiesRenouvelables2019} \\
        \textit{\small{Similar pattern in French-German wind electric generation in Appendix \ref{appendix:a}}}}
    \label{fig:intermitence}
\end{figure}

\paragraph{}

Intermittency brings up the issue of energy storage: to compensate intermittent power sources, one can complete the grid with dispatchable power sources, or "attach" a form of storage alongside intermittent sources of power. Today, the most efficient energy storage is Pumped-hydroelectric storage at 75\% overall efficiency \autocite{capellan-perezAssessingVulnerabilitiesLimits2017}, furthermore \textcite{trainerCanEuropeRun2013} estimates that pumped storage capacity for a 100\% renewably powered EU would have to be multiplied by 20, thus concluding that storage is a major obstacle to Renewable energies.

This combination of intermittency and lack of storage at scale explains why the literature considers the flowing nature of Renewable energies to be a considerable factor in assessing the impact of the transition away from fossil fuels on Global Security \autocites{johanssonSecurityAspectsFuture2013}{scholtenGeopoliticsRenewablesExploring2016}.

%TODO transition

\subsection*{Availability}

Another key factor that will shape the transition is the difference in Renewable Energy potential that different countries and continents have been dealt while having developed drastically different Energy footprints, both illustrated by Figure \ref{fig:availability}.

Standing out is the African continent's considerable Solar energy Potential, especially compared to its primary energy demand. This potential is also present in Australia and the Middle-East. On the other hand, the Renewable Energy potential of many European countries and the PRC is much closer to their consumption.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{static/world_renewable_potential.png}
    \caption{Regional Renewable Energy potential \protect\autocite[227]{teskeEnergyEvolutionSustainable2013} \\
        \textit{\small{Enlarged in Appendix \ref{appendix:b}}}}
    \label{fig:availability}
\end{figure}

% Demand and Supply Security consequences

\subsection*{Grid Communities}

% TODO combine notion of intermitency and availability
While current hydrocarbon fuels are tangible substances, electricity is a drastically more complex to store flow of electrons. The increased complexity of International electric grids due to their nature as a "flow" as well as the aforementioned availability and intermitency will force States into cooperation as "Grid Communities", notably because of increased interdependency, a premise not dissimilar to the foundations of the European Coal and Steel Community \autocites[sec. 3.2]{manssonResourceCurseRenewables2015}[sec. 4.3.1]{scholtenGeopoliticsRenewablesExploring2016}[51]{vandegraafNewWorldGeopolitics2019}. Furthermore, these characteristics of flow rather than stock have also been noted to enable "a market setting that is akin to that of perfect competition and a view of electricity as a commodity instead of strategic good" \autocites[278]{scholtenReliabilityEnergyInfrastructures2013}[sec. 3.2]{manssonResourceCurseRenewables2015}. Thus "renewable resources have a relatively low strategic value because of their physical properties (i.e. geographical distribution, low power density, energy flows rather than stocks that are difficult to accumulate and control over space and time)." \autocite[7]{manssonResourceCurseRenewables2015}

Furthermore, the risk of a physical attack on International Electric grids has been deemed low due in part due to the need for coordinated attacks on particular key choke points in a context of decentralization of Renewable energy generation \autocites{smithstegenTerroristsSunDesertec2012}[43]{vandegraafNewWorldGeopolitics2019}{manssonResourceCurseRenewables2015}. The extent and risk of cyberattacks however will greatly increase. Management of larger electric grids requires more monitoring and automation, much of which is done over networks whose complexity increases the attack surface, making them "easy" targets for attacks especially since attribution is complex, as we have seen carried out \autocites{sunCyberSecurityPower2018}{styczynskiWhenLightsWent2016}{leeAnalysisCyberAttack2016}.

\section*{"Renewable" thirst and hunger}

Abstracting regional renewable energy source availability, research has been conducted on the "Relative Aggregate Footprint" of different sources of energy. \textcite{hadianSystemSystemsApproach2015} (see Figure \ref{fig:raf}) evaluate the sustainability of different energy systems based on their carbon footprint(g \ce{CO2}/kWh), water footprint (m\textsuperscript{3}/GJ), land footprint (m\textsuperscript{2}/GWh) and cost (cents/kWh). Their analysis paints a different picture of the switch to "Renewable" energies, as this section will discuss, "Renewable" Energies imply a balance between potentially conflicting considerations of land area, water consumption, carbon emissions and cost.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{static/ras_hadian_2014.jpg}
    \caption{"System of Systems" Analysis Relative aggregate footprint of different energy sources \\ \protect\autocite{hadianSystemSystemsApproach2015}}
    \label{fig:raf}
\end{figure}

\subsection*{Land footprint}

An important factor is situated at the Food-Energy Nexus: the land footprint of renewable energies. Hydrocarbons are concentrated fossilized solar energy, steering towards a Renewable Energies such as solar power implies a switch to energy highly distributed over the earth's surface \autocite{capellan-perezAssessingVulnerabilitiesLimits2017}. The surface area required by Solar Photovoltaic implies that "there might be a trade-off between land for food, land for energy and land for biodiversity conservation in certain countries" \autocite[772]{capellan-perezAssessingVulnerabilitiesLimits2017}. A theoretical entirely Solar (Photovoltaic) EU-27 with redundant capacity for periods of low yields (e.g. winter) would require over 50\% of its land area dedicated to Solar energy generation \autocite[774]{capellan-perezAssessingVulnerabilitiesLimits2017}.

\textcite{mcdonaldEnergySprawlEnergy2009} estimates the additional land area dedicated to Energy production by 2030 under 2009 United States (US) legislation. The study concludes that an additional at a minimum 206 000 km² of land will be needed for energy generation, that is "regardless of climate change policy" \autocite[4]{mcdonaldEnergySprawlEnergy2009}. The study estimates this figure with optimal land use, thus calculating the land footprint of different energy generation methods notably concluding on the considerable footprint of "Renewables" such as biofuels, wind and hydropower (Figure \ref{fig:land-footprint}).

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{static/land_footprint.png}
    \caption{Land-use intensity for energy production/conservation techniques \\ \protect\autocite{mcdonaldEnergySprawlEnergy2009}}
    \label{fig:land-footprint}
\end{figure}

\subsection*{Water Footprint}

Analysis of the water usage of different technologies of the energy mix  in a scenario of transition to Renewables has estimated "an increase of 37\%–66\%, during the next two decades, in the amount of water required for total energy production in the world" \autocite[4684]{hadianWaterDemandEnergy2013}. However, it has been noted that Solar (Photovoltaic), Wind and Geothermal electric generation will bring about a reduced water consumption and withdrawal compared to Nuclear, Coal and natural Gas electric generation \autocites[67-72]{ferroukhiRenewableEnergyWater2015}{meldrumLifeCycleWater2013}.

Other studies paint a far worrying picture by taking into account the water footprint of Hydroelectric power and Biofuels, "the IEA Alternative Policy Scenario, for example, gives a bioenergy share of 11\% in global energy consumption [...] it is expected that the global annual biofuel WF will increase more than tenfold, from about 90 km3/year in 2005 to 970 km3/year in 2030." \autocite[776,773]{gerbens-leenesBiofuelScenariosWater2012}.

This is explained, notably by a previous study of the same author that evaluates the water footprint of different energy sources see Figure \ref{fig:water-footprint}. To put this in perspective, \textcite{hanjraGlobalWaterCrisis2010} shows that with current population estimates we can approximate a 3300 km3/year water production/consumption gap by 2050, "if not filled this water gap will leave a food gap and affect global food security." \autocite[370]{hanjraGlobalWaterCrisis2010}.

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
                ybar,
                ymin=0,
                ylabel={Water footprint (m3/GJ)},
                x tick label style={rotate=90},
                xtick=data,
                width=0.8\textwidth,
                height=0.25\textheight,
                xticklabels={{Wind},{Nuclear},{Natural gas},{Coal},{Solar thermal},{Crude oil},{Hydropower},{Biomass}},
            ]
            \addplot table [x expr=\coordindex,y={Average water footprint (m3/GJ)}, col sep=comma] {data/water_footprint.csv};
        \end{axis}
    \end{tikzpicture}
    \caption{Average water footprint by energy source \\ Data from \protect\textcite{gerbens-leenesWaterFootprintEnergy2009}}
    \label{fig:water-footprint}
\end{figure}

\subsection*{The food-water-energy nexus}

Projections find that "by 2040, 2.5-3.5 times more food will be needed at the global level" \autocite[81]{devriesNaturalResourcesLimits1995}. The land and water footprint of some Renewables, especially Hydroelectric and Biofuel production, have and will significantly challenge this objective. \textcite[65]{debnathChapterInteractionBiofuels2019}'s research concludes that "the link between the biofuels and agricultural markets is two way. The changes in agricultural feed-stock prices can also lead to changes in the production of biofuels and vice versa", an assessement illustrated by Figure \ref{fig:food-biodiesel}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{static/biofuel-food-linkage.jpg}
    \caption{"Change in world biodiesel price, world crude oil price, and world vegetable oil price, \% change year-to-year." \\ \protect\autocite[65]{debnathChapterInteractionBiofuels2019}}
    \label{fig:food-biodiesel}
\end{figure}

Projections of the effect of Biofuels on wheat prices show an estimated increase in the range of 50-200\% \autocite{johanssonScenarioBasedAnalysis2007}. Biofuels are essentially becoming cash crops, estimates point to their growth being attributable to close to 8Mha of the 17Mha increase in cultivated land area between 2000 and 2008 \autocite{prielerLandFoodFuel2013}. A notable illustration of the perverse effects of cash crops is their contribution to the Yemenite humanitarian crisis where they have engulfed a significant share of the country's dwindling water reserves \autocite{variscoPumpingYemenDry2019}.

Causes of conflict are complex to pinpoint, yet studies have linked water scarcity as a contributing factor to the Syrian civil war that erupted in 2011 \autocite{breisingerEconomicImpactsClimate2013,gleickWaterDroughtClimate2014}. \textcite{gleickWaterDroughtClimate2014} cites consistently drier winters in Syria since 1990, worsened by Climate change, as the cause for nationwide food insecurity whose effects were exacerbated in a context of demographic boom. The article also underlines the capture of the Tabqa hydroelectric dam as an illustration of water as a strategic resource. The article's depiction of the water and food shortages as the igniter of the Syrian conflict paints a new perspective on the large scale deployment of thirsty and hungry Renewable Energies. While noting that such events are \textit{always} a combination of multiple factors, perhaps \textcite{gleickWaterDroughtClimate2014}'s mention of the US Diplomatic Cable relaying an informant's prophetic description of the "water problem" in Syria could be an illustration of what is to come.

\begin{quote}
    "the perfect storm", a confluence of drought conditions with other economic and social pressures that [...] could undermine stability in Syria.

    % quote source
    \begin{flushright}
        \textcite{2008DroughtAppeal2008}
    \end{flushright}
\end{quote}

\section*{"Critical Raw Materials"}

The Mining industry will be key for the manufacturing of Renewable energy generation infrastructure, it also holds a significant water footprint, especially specific Rare Earth elements, that is bound to increase through the transition to Renewables \autocite{blanpainREWAS2016Materials2016}. This is an issue acknowledged by the IEA: "[the] mining industry energy and water intensities are also likely to increase in the future as a result of exploitation of lower-grade ores" \autocite[183]{internationalenergyagencyRenewables2019Analysis2019}.

Europium, Terbium, Dysprosium and Yttrium are all Heavy Rare Earth Elements (HREE), a subset of so called "Rare Earth" elements which are in scarce supply in the Earth's crust, they also happen to be critical to clean energy generation in the short and medium term \autocite{smithstegenHeavyRareEarths2015}. These elements are notably needed for the manufacturing of permanent magnets used in wind turbines, their economic importance coupled with their supply risk have led the EU and the US to designate them as "Critical Raw Materials" (CRM) \autocites{europeancommissionStudyReviewList2017}{u.s.departmentofenergyDepartmentEnergyCritical2011}. Many of these minerals (not just HREE) have no current viable alternative for their uses, furthermore, the EU and the US are considerably behind in commercial-grade post-extraction supply chain for processing extracted HREE \autocites{europeancommissionStudyReviewList2017}[6]{smithstegenHeavyRareEarths2015}{u.s.departmentofenergyDepartmentEnergyCritical2011}.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{static/iea_renewable_minerals.png}
    \caption{"Cumulative extraction of selected metals for the renewable electricity sector over 2013-18 and 2019-24" \\ \protect\autocite[182]{internationalenergyagencyRenewables2019Analysis2019}}
    \label{fig:crm-renewables}
\end{figure}

"Rare Earths" are only one aspect of Renewable Energies' race to minerals. Indeed "for mineral resources (excluding fossil fuels), the \ang{2}C climate target generates a cumulative total use that is [...] 31\% higher over the 2010–2100 period compared to the baseline scenario. [...] Regarding the metals only, the cumulative total use is [...] 46\% higher over the 2010–2100 period." \autocite[5]{boubaultDevisingMineralResource2019}. This shift from fossil fuel to mineral extraction  is reflected in Figure \ref{fig:crm-renewables}, which illustrates the projected increased extraction of Rare earths but also copper (electronic circuitry), cobalt and lithium (Lithium-ion battery) or tantalum (Solar Photovoltaic) amongst other key minerals for the renewable energy industry \autocites{internationalenergyagencyRenewables2019Analysis2019}{scholtenGeopoliticsRenewablesExploring2016}.

\paragraph{}

The EU is dependent on the PRC as its main supplier for 60\% of its CRM \autocite{europeancommissionStudyReviewList2017}. This dependence is reflected elsewhere on the Globe, notably as experienced by Japan when the PRC halted, then implemented quotas on its Rare Earth exports to the Nippon islands following the Japanese arrest of a Chinese fisherman in the South China Sea \autocites{klareChapterRareEarths2012}{seamanChineTerresRares2019}. These restrictions have been applied on numerous occasions to different countries \autocite[26]{seamanChineTerresRares2019}, furthermore, in the US, Renewable Energies "account for about 20 percent of the consumption of [Rare Earth Elements]" \autocite{klareChapterRareEarths2012} emphasizing the strategic role of these CRM.

"As the supply side has been unable to keep up with the demand side, mineral prices have skyrocketed" \autocite[21]{deridderGeopoliticsMineralResources2013}. This restriction of supply has been attributed to the Chinese protectionist export policies on CRM, especially rare earths, which has "distorted the market principles" of International Free Trade. While the World Trade Organization's dispute settlement mechanisms have allowed for a peaceful resolution of the Sino-Nippon conflict, the monopolistic tendencies of the Chinese Rare Earth extraction industry and its consequences on supply have been brought to light \autocites{deridderGeopoliticsMineralResources2013}{seamanChineTerresRares2019}.

These signs of scarcity of supply and ensuing State intervention into what is deemed a "critical" sector are ominously reminiscent of the very industry Renewables are trying to replace: Oil. As the extraction of relatively accessible supplies of minerals nears its end, the diminishing Energy Return on Investement (ERI), increasingly polluting and expensive extraction of CRM as well as the declining ore grades (concentration of a particular mineral or metal) have led some scholars to derive the notion of "peak oil" into "peak minerals" \autocite{priorResourceDepletionPeak2012}.

\paragraph{}

This "Securitization" of the question of mineral supplies for Renewable Energies has been toned down by some scholars who's rhetoric rather echoes that of "Riskification" \autocite{gholzRareEarthElements2014,mossCriticalMetalsPath2013,overlandGeopoliticsRenewableEnergy2019}. They underline European self-sufficiency of some CRM, the discovery of deposits outside of China, progress in recycling efficiency as well as research into substitutes to CRM \autocite[153]{mossCriticalMetalsPath2013}. Furthermore, Chinese quotas on Rare Earth exports have been akined to a policy measure for the reduction of polluting domestic production rather than a geopolitical motive \autocite{wubbekeRareEarthElements2013}. It has also been noted that the Chinese embargo on exports of Rare Earths to Japan has only stimulated the development of market competition \autocite{gholzRareEarthElements2014}, echoing a reemerging discourse of trust in "Market Forces", emphasizing that CRM "are simply another commodity [...] unable to transcend the realities of economics" \autocite[37]{overlandGeopoliticsRenewableEnergy2019}.

As we have seen however, CRM extraction and processing is far from a banal "commodity", they are essential to the Renewable Energy transition rendered critically urgent by the Climate crisis. Furthermore, Rare Earth elements supply chains are particularly costly and research intensive, due to the complexity the chemical processes involved, suggesting that the "Market Forces" will just force non-Chinese industries to "catch-up" is ignorant of the fact that such long term investments require State support \autocites{smithstegenHeavyRareEarths2015}{klareChapterRareEarths2012}. The same can be said of CRM recycling:  these elements are used in very small densities and their recycling rate stands at a mere 1\% in the EU and Australia \autocites{liuRareEarthsShades2016}{klareChapterRareEarths2012}.

Perhaps the Securitization discourse on CRM is in part a self-fulfilling prophecy, this is an entire discussion by itself. The fact remains that there is an undeniable move to secure CRM, China may have limited its domestic production but it is buying strategic companies in Rare Earth extraction, giving it a notable presence in Greenland the "Eldorado for REEs" \autocite[286]{peterChinaRareEarth2014}. Furthermore dependent Nations' "wake up call" on Chinese dominance of the Rare Earths market demonstrates in and of itself the strategic interest in CRM thus slowly restarting non-Chinese CRM industries, research into recycling and alternatives, however slow it may be \autocites{smithstegenHeavyRareEarths2015}{klareChapterRareEarths2012}.


\section*{Conclusions}

The transition to Renewable Energies implies a growing use of electric power as a conduit for energy, thus drastically changing the dynamics of Securitization of Energy. The intermittency of most renewable electric sources coupled with the lack of large scale efficient storage will shift electric grids away from the paradigm of dispatchable power output instilled by Fossil and nuclear generation technologies. There will thus be a need for larger scale, international or even continental electric grids to "smooth" the consumption and production curves. % demand and supply security
Actors of electric grids will become interdependent, forcing multilateral cooperation and thus rendering conflict economically unviable.

The transition will not however only consist of electric power, projections also show a strong reliance on Biofuels as a large source of energy. Followed by hydroelectric power, biofuels are by far the most water intensive source of energy. They also compete with food production for arable land surface area and soil exhaustion, thus influencing World food markets. Considerable insecurity lies at the food-water-energy nexus, as we've seen, water and related food shortages have played a role in the Syrian civil war.

Lastly, the infrastructure required for harvesting Renewables Energies, specifically for electricity generation, requires particular metals and minerals for its manufacturing. Some subsist in abundant quantities in the Earth's crust. However, security concerns over what has been called "Critical Raw Materials" have expanded, notably with the beginning of the transition to renewable energies. Economic but also strategic dependence on a handful of key actors' mining and processing industries have been the sources of Geopolitical tensions amidst restrictions on CRM trade.

\paragraph{}

Climate Change and depletion of fossil fuel supplies are forcing the expansion of Renewable Energies, the impact of Global Warming on the Syrian crisis is a mere introduction to the consequences this inexorable transition seeks to avoid. Given this inevitability, we have assessed the challenges on Global Security caused by this shift to Renewables. We have shown the need for vast electric grids and their potential for hindering conflict through interdependence of actors. However, assuming the continuation of the demographic and economic growth paradigm, the enduring water and land footprints of dominant Renewables, as well as constraining dependence on rare materials, the expansion of Renewables clearly challenges Global Security. Indeed, unless rising Energy demands are curtailed, the expansion of Renewables will only mutate the progressively worsening Global Security consequences of Human growth.


%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\newpage

% \emergencystretch=1em

\section*{References}

\printbibliography[heading=none]

\vspace*{\fill}
% print license
\doclicenseThis


%----------------------------------------------------------------------------------------
%	APPENDIX
%----------------------------------------------------------------------------------------

\newpage

\appendix
\appendixpage
\addappheadtotoc

\section{Regional Renewable Energy potential}
\label{appendix:b}
\begin{figure}[H]
    \centering
    \includegraphics[height=0.84\textwidth, angle=90]{static/world_renewable_potential.png}
    \caption{Enlarged Regional Renewable Energy potential \protect\autocite{teskeEnergyEvolutionSustainable2013}}
\end{figure}

\section{Hourly load on German and French wind farms in 2016}
\label{appendix:a}
\begin{figure}[H]
    \includegraphics[width=\textwidth,trim={1mm 1mm 1mm 1mm},clip=true]{static/ger-fr_wind.jpg}
    \caption{Hourly load on German and French wind farms in 2016 \protect\autocite{jancoviciImpactEnergiesRenouvelables2019}}
\end{figure}

%----------------------------------------------------------------------------------------

\end{document}
